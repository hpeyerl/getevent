#!/usr/bin/python
# Got this from here and modified it: https://stackoverflow.com/questions/5060710/format-of-dev-input-event
#
import struct
import time
import sys


infile_path = "/dev/input/event" + (sys.argv[1] if len(sys.argv) > 1 else "0")

#long int, long int, unsigned short, unsigned short, unsigned int
FORMAT = 'llhhI'
EVENT_SIZE = struct.calcsize(FORMAT)

#open file in binary mode
in_file = open(infile_path, "rb")

event = in_file.read(EVENT_SIZE)

# From https://elixir.bootlin.com/linux/v4.9.20/source/include/uapi/linux/input-event-codes.h
REL_X = 0
REL_Y = 1
TYPE_TOUCHPAD = 3
TYPE_MOUSE = 2

while event:
    (tv_sec, tv_usec, type, code, value) = struct.unpack(FORMAT, event)

    if type == TYPE_MOUSE and code <= REL_Y and value != 0:
        code_str = hex(code)
        if code == REL_X:
            code_str = "X"
        if code == REL_Y:
            code_str = "Y"
        if value > 2^32:
            value = value - 0xffffffff
        print("Event type %u, code %s, value %u at %d.%d" % (type, code_str, value, tv_sec, tv_usec))

    event = in_file.read(EVENT_SIZE)

in_file.close()

